import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  private apiUrl: string = environment.url;
  httpHeaders: HttpHeaders;
  constructor( private http: HttpClient){
    this.httpHeaders = new HttpHeaders({
      'Content-type': 'application/json'
    });
  }
  getList(url): any{
    console.log("hola url"+ url);
    return this.http.get(url , {headers: this.httpHeaders});
  }
}
