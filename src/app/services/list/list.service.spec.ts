import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, inject, TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { ListService } from './list.service';

describe('ListService', () => {
  let service: ListService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
      ,
      providers: [
        ListService
      ]
    });
    injector = getTestBed();
    httpMock = injector.inject(HttpTestingController);
    service = TestBed.inject(ListService);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('Debe obtener los datos de character',  inject([ListService], (service: ListService) => {
    const mockResponse = [
      {
        "id": 361,
        "name": "Toxic Rick",
        "status": "Dead",
        "species": "Humanoid",
        "type": "Rick's Toxic Side",
        "gender": "Male",
        "origin": {
          "name": "Alien Spa",
          "url": "https://rickandmortyapi.com/api/location/64"
        },
        "location": {
          "name": "Earth",
          "url": "https://rickandmortyapi.com/api/location/20"
        },
        "image": "https://rickandmortyapi.com/api/character/avatar/361.jpeg",
        "episode": [
          "https://rickandmortyapi.com/api/episode/27"
        ],
        "url": "https://rickandmortyapi.com/api/character/361",
        "created": "2018-01-10T18:20:41.703Z"
      }
    ];
    const url = environment.url + 'character';
    service.getList(url).subscribe(result=>{
      expect(result[0].name).toEqual('Toxic Rick');
    });
    httpMock.expectOne(url).flush(mockResponse);
  }));
});
