import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class ListMockService {
    tech = {
        tech: 'Node',
        year: '2009',
        author: 'Ryan Dahl',
        license: 'MIT',
        language: 'JavaScript',
        type: 'Back-End',
        logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/220px-Node.js_logo.svg.png'
      };
    listMock = [
        this.tech
    ];
    httpHeaders: HttpHeaders;
}
