import { ActionReducerMap } from '@ngrx/store';
import { listReducer, ListState } from './list/reducer/list.reducer';


export interface AppState{    
    list: ListState;
  }

export const appReducers: ActionReducerMap<AppState> = {
    list: listReducer
};
