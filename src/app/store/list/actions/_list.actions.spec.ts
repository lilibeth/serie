import * as fromList from './list.actions';
describe('Deberia ejecutar la acción de CargarList' , () => {
    it('fromList.CARGAR_LIST', () => {
        const accion = new fromList.CargarList("");
        expect(accion.type).toEqual(fromList.CARGAR_LIST);
    });
});
describe('Deberia ejecutar la acción de CargarListSuccess' , () => {
    it('fromList.CARGAR_LIST_SUCCESS', () => {
        const List = [];
        const accion = new fromList.CargarListSuccess(List);
        expect(accion.type).toEqual(fromList.CARGAR_LIST_SUCCESS);
    });
});
describe('Deberia ejecutar la acción de CargarListFail' , () => {
    it('fromList.CARGAR_LIST_FAIL', () => {
        const payload = [];
        const accion = new fromList.CargarListFail(payload);
        expect(accion.type).toEqual(fromList.CARGAR_LIST_FAIL);
    });
});
