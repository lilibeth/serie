import { Action } from '@ngrx/store';
export const CARGAR_LIST = '[List] Cargar list';
export const CARGAR_LIST_FAIL = '[List] Cargar list fail';
export const CARGAR_LIST_SUCCESS = '[List] Cargar list success';
export class CargarList implements Action {
    readonly type = CARGAR_LIST;
    constructor(public data:any){}
}
export class CargarListFail implements Action {
    readonly type = CARGAR_LIST_FAIL;
    constructor(public payload: any){}
}
export class CargarListSuccess implements Action {
    readonly type = CARGAR_LIST_SUCCESS;
    constructor(public data: any){}
}
export type listAcciones =  CargarList |
                            CargarListFail |
                            CargarListSuccess ;
