import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { ListService } from 'src/app/services/list/list.service';
import * as listActions from 'src/app/store/list/actions/list.actions';

@Injectable()
export class ListEffects {

    constructor(private actions$: Actions, public listService: ListService){}
    @Effect()
    cargarList$ = this.actions$.pipe(ofType( listActions.CARGAR_LIST ), switchMap( action => {                               
                               return this.listService.getList(action['data']).pipe(
                                                  map( list =>  new listActions.CargarListSuccess( list )),
                                                  catchError(Error => {
                                                    let NError = Error;
                                                    const keyError  = 'error';
                                                    const keyErros = 'errors';
                                                    if(Error[keyError] && Error[keyError][keyErros]){
                                                        NError = Error[keyError][keyErros][0];
                                                    } 
                                                    return of( new listActions.CargarListFail( NError ));
                                                   }));})); 
}

