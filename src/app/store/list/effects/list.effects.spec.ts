import { Observable, of, throwError } from 'rxjs';
import { ListMockService } from 'src/app/mocks/list';
import { ListService } from 'src/app/services/list/list.service';
import { ListEffects } from './list.effects';
import { Mock, provideMagicalMock } from 'angular-testing-library/src/service_mock';
import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Actions } from '@ngrx/effects';
import { CargarList, CargarListFail, CargarListSuccess } from '../actions/list.actions';
import { cold } from 'jasmine-marbles';


describe('list Effects', () => {
    let effects: ListEffects;
    let listService: Mock<ListService>;
    let actions: Observable<any>;
    const techs = {
        items: new ListMockService().listMock,
    };
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ListEffects,
                provideMagicalMock(ListService),
                provideMockActions(() => actions)],
        });
        actions = TestBed.inject(Actions);
        effects = TestBed.inject(ListEffects);
        listService = TestBed.get(ListService);
    });
    it('Deberia crear el effect list', () => {
        expect(effects).toBeTruthy();
    });
    it('List Effects Test --> CargarList / CargarListSuccess', () => {
        const action = new CargarList("");
        const result = new CargarListSuccess(techs.items as any );
        actions = of(action);
        const response = cold('-a|', { a: techs.items });
        const expected = cold('-b|', { b: result });
        listService.getList.and.returnValue(response);
        expect(effects.cargarList$).toBeObservable(expected);
    });
    it('List Effects Test --> CargarList / CargarListFail', () => {
       const action = new CargarList("");
       actions = of(action);
       listService.getList.and.returnValue(throwError({
           error: {
               errors: [
                   {
                       code: 801,
                       detail: '¡Lo sentimos!-No es posible continuar con la transacción en este momento.-Por favor contáctenos por alguno de nuestros canales de atención. CAR',
                       id: '20190822110141871871',
                       timestamp: '2019082211012959',
                       title: 'Transacción no autorizada.',
                       type: 'SPLCONVENIOSPORNITRS',
                       status: 500
                   }
               ]
           }
       }));
       const expected = cold('(b|)', { b: new CargarListFail({
           code: 801,
           detail: '¡Lo sentimos!-No es posible continuar con la transacción en este momento.-Por favor contáctenos por alguno de nuestros canales de atención. CAR',
           id: '20190822110141871871',
           timestamp: '2019082211012959',
           title: 'Transacción no autorizada.',
           type: 'SPLCONVENIOSPORNITRS',
           status: 500
       })});
       expect(effects.cargarList$).toBeObservable(expected);
   });
});
