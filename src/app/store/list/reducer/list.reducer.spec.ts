import { estadoInicialList, listReducer } from '../reducer/list.reducer';
import * as listActions from '../actions/list.actions';

describe('Estado inicial - Reducer List', () => {

    it('Deberia retornar el estado inicial del reducer', () => {
        const action = {} as any;
        const result = listReducer(estadoInicialList, action);
        expect(result).toBe(estadoInicialList);
    });

    it('listActions tipo: CARGAR_LIST', () => {
        const action = new listActions.CargarList("");
        const result = listReducer(estadoInicialList, action);
        expect(result).toEqual(
            {
            ...estadoInicialList,
            loading: true,
            error: null
            });
    });

    it('listActions tipo: CARGAR_LIST_SUCCESS', () => {
        const data = [];
        const action = new listActions.CargarListSuccess(data);
        const result = listReducer(estadoInicialList, action);
        expect(result).toEqual(
            {
            ...estadoInicialList,
            loading: false,
            loaded: true,
            data: [...action.data]
           }
        );
    });

    it('listActions tipo: CARGAR_LIST_FAIL', () => {
        const payload = [];
        const action = new listActions.CargarListFail(payload);
        const result = listReducer(estadoInicialList, action);
        expect(result).toEqual(
            {
                ...estadoInicialList,
                loaded: false,
                loading: false,
                error: {
                    status: action.payload.status,
                    message: action.payload.message,
                    url: action.payload.url

                }
           }
        );
    });
});
