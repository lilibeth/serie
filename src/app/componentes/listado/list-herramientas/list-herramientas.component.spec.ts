import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { Route, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TestStore } from 'src/app/mocks/teststore';
import { ListService } from 'src/app/services/list/list.service';
import { AppState } from 'src/app/store/app.reducer';
import { ListEffects } from 'src/app/store/list/effects/list.effects';
import { Location } from '@angular/common';
import { ListHerramientasComponent } from './list-herramientas.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store, StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMagicalMock } from 'angular-testing-library/src/service_mock';
import { Actions } from '@ngrx/effects';
import { CargarList } from 'src/app/store/list/actions/list.actions';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';

describe('ListHerramientasComponent', () => {
  let component: ListHerramientasComponent;
  let fixture: ComponentFixture<ListHerramientasComponent>;
  let store: TestStore<AppState>;
  let effects: ListEffects;
  let actions: Observable<any>;
  let queryService: ListService;
  let httpClient: HttpClient;
  let router: Route;
  let location: Location;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHerramientasComponent ],
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({}),
        RouterTestingModule.withRoutes([
          { path: 'home', component: ListHerramientasComponent }
        ])            
      ],
      providers: [
        {
        provide: Store,
        useClass: TestStore,
        ListService
      },
        ListEffects,
      provideMockActions(() => actions),
      provideMagicalMock(ListService),
      provideMagicalMock(NgbModal),
      ]
    })
    .compileComponents();
    actions = TestBed.inject(Actions);
    effects = TestBed.inject(ListEffects);
    queryService = TestBed.inject(ListService);
  }));
  beforeEach(inject([Store], (testStore: TestStore<AppState>) => {
    store = testStore;
    store.setState({ data: {
      results : [],
      info: {
        next: "",
        prev: ""
      }
    } });
    fixture = TestBed.createComponent(ListHerramientasComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    location = TestBed.inject(Location);
    fixture.detectChanges();
    queryService = new ListService(httpClient);
  }));
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Deberia disparar la accion CargarList in el ngOnInit', () => {
    fixture.detectChanges();
    const action = new CargarList("");
    actions = of(action);
    const StoreR = TestBed.get(Store);
    const spy = spyOn(store, 'dispatch');
    StoreR.dispatch();
    expect(spy).toHaveBeenCalled();
    expect(effects).toBeTruthy();
 });
  
  
  it('Deberia ordenar un array de objectos por el tech, decendente', () => {
      const tecnologias = [
        {
          name: 'Node',
          year: '2009',
          author: 'Ryan Dahl',
          license: 'MIT',
          language: 'JavaScript',
          type: 'Back-End',
          logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/220px-Node.js_logo.svg.png'
        },
        {
          name: 'React',
          year: '2013',
          author: 'Jordan Walke',
          license: 'MIT',
          language: 'JavaScript',
          type: 'Front-End',
          logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/220px-React-icon.svg.png'
        }
      ];
      component.herramientas = tecnologias;
      fixture.detectChanges();
      component.oderDeccendente();
      expect(component.herramientas).toEqual(tecnologias);
  });

  it('Deberia ordenar un array de objectos por el tech, acendentemente', () => {
        const tecnologias = [
          {
            "id": 361,
            "name": "Toxic Rick",
            "status": "Dead",
            "species": "Humanoid",
            "type": "Rick's Toxic Side",
            "gender": "Male",
            "origin": {
              "name": "Alien Spa",
              "url": "https://rickandmortyapi.com/api/location/64"
            },
            "location": {
              "name": "Earth",
              "url": "https://rickandmortyapi.com/api/location/20"
            },
            "image": "https://rickandmortyapi.com/api/character/avatar/361.jpeg",
            "episode": [
              "https://rickandmortyapi.com/api/episode/27"
            ],
            "url": "https://rickandmortyapi.com/api/character/361",
            "created": "2018-01-10T18:20:41.703Z"
          },
          {
            "id": 362,
            "name": "Andre Rick",
            "status": "Dead",
            "species": "Humanoid",
            "type": "Rick's Toxic Side",
            "gender": "Male",
            "origin": {
              "name": "Alien Spa",
              "url": "https://rickandmortyapi.com/api/location/64"
            },
            "location": {
              "name": "Earth",
              "url": "https://rickandmortyapi.com/api/location/20"
            },
            "image": "https://rickandmortyapi.com/api/character/avatar/361.jpeg",
            "episode": [
              "https://rickandmortyapi.com/api/episode/27"
            ],
            "url": "https://rickandmortyapi.com/api/character/361",
            "created": "2018-01-10T18:20:41.703Z"
          }
        ];
        const expectTecnologias = [
          {
            "id": 362,
            "name": "Andre Rick",
            "status": "Dead",
            "species": "Humanoid",
            "type": "Rick's Toxic Side",
            "gender": "Male",
            "origin": {
              "name": "Alien Spa",
              "url": "https://rickandmortyapi.com/api/location/64"
            },
            "location": {
              "name": "Earth",
              "url": "https://rickandmortyapi.com/api/location/20"
            },
            "image": "https://rickandmortyapi.com/api/character/avatar/361.jpeg",
            "episode": [
              "https://rickandmortyapi.com/api/episode/27"
            ],
            "url": "https://rickandmortyapi.com/api/character/361",
            "created": "2018-01-10T18:20:41.703Z"
          },
          {
            "id": 361,
            "name": "Toxic Rick",
            "status": "Dead",
            "species": "Humanoid",
            "type": "Rick's Toxic Side",
            "gender": "Male",
            "origin": {
              "name": "Alien Spa",
              "url": "https://rickandmortyapi.com/api/location/64"
            },
            "location": {
              "name": "Earth",
              "url": "https://rickandmortyapi.com/api/location/20"
            },
            "image": "https://rickandmortyapi.com/api/character/avatar/361.jpeg",
            "episode": [
              "https://rickandmortyapi.com/api/episode/27"
            ],
            "url": "https://rickandmortyapi.com/api/character/361",
            "created": "2018-01-10T18:20:41.703Z"
          }
        ];
        component.herramientas = tecnologias;
        fixture.detectChanges();
        component.oderAccendente();
        expect(component.herramientas).toEqual(expectTecnologias);
    });
});
