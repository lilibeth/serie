import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/store/app.reducer';
import { CargarList } from 'src/app/store/list/actions/list.actions';

import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-herramientas',
  templateUrl: './list-herramientas.component.html',
  styleUrls: ['./list-herramientas.component.css']
})
export class ListHerramientasComponent implements OnInit , OnDestroy{  
  subscription: Subscription;
  searchForm: FormGroup;  
  herramientas;
  copiaHerramientas;
  total;
  menuActive = true;
  closeResult: string;
  urlNext;
  urlPrev;

  constructor( public store: Store<AppState>, private formBuilder: FormBuilder, private modalService: NgbModal) {
    this.createFormSearch();
  }

  createFormSearch(): void {
    this.searchForm = this.formBuilder.group(
      {
        work: [null],
        typeSearch: ['1']
      }
    );
    this.searchForm.valueChanges.subscribe(data => {
      this.search(data);
     });
  }
  

  ngOnInit(): void {     
    let url = "https://rickandmortyapi.com/api/character/?page=1";
    this.store.dispatch(new CargarList(url));
    this.store.select('list').subscribe(login => {      
      if (login){
        if (login.data !== null) {
          const data = [];
          this.herramientas = login.data.results;
          this.herramientas.forEach((n: any) => {
              data.push({
                ...n,
                show: false
              });
            });
          this.herramientas = data;
          this.copiaHerramientas = [...data];
          this.total = this.herramientas.length;
          this.urlNext = login.data.info.next;
          this.urlPrev = login.data.info.s
        }
      }
    });
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  } 


  showContent(position): void{
    const data = [];
    this.herramientas.forEach((n: any, i) => {
        if (i === position) {
          n.show = true;
        }
        data.push(n);
      });
    this.herramientas = data;
    this.copiaHerramientas = [...data];
  }

  hideContent(position): void{
    const data = [];
    this.herramientas.forEach((n: any, i) => {
        if (i === position) {
          n.show = false;
        }
        data.push(n);
      });
    this.herramientas = data;
    this.copiaHerramientas = [...data];
  }

  search(data): void{
    if ( data.typeSearch === '1' && data.work !== ''){
      this.herramientas = this.herramientas.filter(h => h.name.indexOf(data.work) >= 0);
      this.total = this.herramientas.length;
    }    
    if ( data.work === '' ){
      this.herramientas = [...this.copiaHerramientas];
      this.total = this.herramientas.length;
    }
  }


  oderDeccendente(): void{
    this.herramientas.sort((a, b) => {
        if (a.name < b.name) {
          return 1;
        }
        if (a.name > b.name) {
          return -1;
        }
        return 0;
      });
  }
  oderAccendente(): void{
    this.herramientas.sort((a, b) => {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });
  }  

  open(content) {
    this.modalService.open( content , {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  nexPage(){    
    let url = this.urlNext;
    this.store.dispatch(new CargarList(url));
    this.store.select('list').subscribe(login => {
      console.log(login);
      if (login){
        if (login.data !== null) {
          const data = [];
          this.herramientas = login.data.results;
          this.herramientas.forEach((n: any) => {
              data.push({
                ...n,
                show: false
              });
            });
          this.herramientas = data;
          this.copiaHerramientas = [...data];
          this.total = this.herramientas.length;
          this.urlNext = login.data.info.next;
          this.urlPrev = login.data.info.prev
        }
      }
    });

  }

  prevPage(){
    let url = this.urlPrev;
    this.store.dispatch(new CargarList(url));
    this.store.select('list').subscribe(login => {
      console.log(login);
      if (login){
        if (login.data !== null) {
          const data = [];
          this.herramientas = login.data.results;
          this.herramientas.forEach((n: any) => {
              data.push({
                ...n,
                show: false
              });
            });
          this.herramientas = data;
          this.copiaHerramientas = [...data];
          this.total = this.herramientas.length;
          this.urlNext = login.data.info.next;
          this.urlPrev = login.data.info.prev
        }
      }
    });

  }
}
