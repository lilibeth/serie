import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListHerramientasComponent } from './list-herramientas/list-herramientas.component';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { listEffectsArr } from 'src/app/store/list/effects';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


const routes: Routes = [
  { path: '', component: ListHerramientasComponent },
  { path: 'home', component: ListHerramientasComponent },
];

@NgModule({
  declarations: [ListHerramientasComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    EffectsModule.forRoot(listEffectsArr),
    FormsModule,
    ReactiveFormsModule,
    NgbModule
  ]
})
export class ListadoModule { }
